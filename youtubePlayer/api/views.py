from .models import History, Bookmark
from rest_framework import viewsets
from .serializers import HistorySerializer, BookmarkSerializer


class HistoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = History.objects.all()
    serializer_class = HistorySerializer


class BookmarksViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer